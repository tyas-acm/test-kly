# test-kly
SIMPLE PROGRAM (GUESTBOOK) with Laravel


## Getting started

1. Download or clone this repository
2. Create a new database with name "guest_book"
3. Execute SQL file in database folder
4. Run this project with "php artisan serve" in terminal
5. Login with username = "admin@mail.com" and password = "admin"