/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : guest_book

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 09/01/2022 14:16:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES (1101, 'SIMEULUE');
INSERT INTO `city` VALUES (1102, 'ACEH SINGKIL');
INSERT INTO `city` VALUES (1103, 'ACEH SELATAN');
INSERT INTO `city` VALUES (1104, 'ACEH TENGGARA');
INSERT INTO `city` VALUES (1105, 'ACEH TIMUR');
INSERT INTO `city` VALUES (1106, 'ACEH TENGAH');
INSERT INTO `city` VALUES (1107, 'ACEH BARAT');
INSERT INTO `city` VALUES (1108, 'ACEH BESAR');
INSERT INTO `city` VALUES (1109, 'PIDIE');
INSERT INTO `city` VALUES (1110, 'BIREUEN');
INSERT INTO `city` VALUES (1111, 'ACEH UTARA');
INSERT INTO `city` VALUES (1112, 'ACEH BARAT DAYA');
INSERT INTO `city` VALUES (1113, 'GAYO LUES');
INSERT INTO `city` VALUES (1114, 'ACEH TAMIANG');
INSERT INTO `city` VALUES (1115, 'NAGAN RAYA');
INSERT INTO `city` VALUES (1116, 'ACEH JAYA');
INSERT INTO `city` VALUES (1117, 'BENER MERIAH');
INSERT INTO `city` VALUES (1118, 'PIDIE JAYA');
INSERT INTO `city` VALUES (1171, 'BANDA ACEH');
INSERT INTO `city` VALUES (1172, 'SABANG');
INSERT INTO `city` VALUES (1173, 'LANGSA');
INSERT INTO `city` VALUES (1174, 'LHOKSEUMAWE');
INSERT INTO `city` VALUES (1175, 'SUBULUSSALAM');
INSERT INTO `city` VALUES (1201, 'NIAS');
INSERT INTO `city` VALUES (1202, 'MANDAILING NATAL');
INSERT INTO `city` VALUES (1203, 'TAPANULI SELATAN');
INSERT INTO `city` VALUES (1204, 'TAPANULI TENGAH');
INSERT INTO `city` VALUES (1205, 'TAPANULI UTARA');
INSERT INTO `city` VALUES (1206, 'TOBA SAMOSIR');
INSERT INTO `city` VALUES (1207, 'LABUHAN BATU');
INSERT INTO `city` VALUES (1208, 'ASAHAN');
INSERT INTO `city` VALUES (1209, 'SIMALUNGUN');
INSERT INTO `city` VALUES (1210, 'DAIRI');
INSERT INTO `city` VALUES (1211, 'KARO');
INSERT INTO `city` VALUES (1212, 'DELI SERDANG');
INSERT INTO `city` VALUES (1213, 'LANGKAT');
INSERT INTO `city` VALUES (1214, 'NIAS SELATAN');
INSERT INTO `city` VALUES (1215, 'HUMBANG HASUNDUTAN');
INSERT INTO `city` VALUES (1216, 'PAKPAK BHARAT');
INSERT INTO `city` VALUES (1217, 'SAMOSIR');
INSERT INTO `city` VALUES (1218, 'SERDANG BEDAGAI');
INSERT INTO `city` VALUES (1219, 'BATU BARA');
INSERT INTO `city` VALUES (1220, 'PADANG LAWAS UTARA');
INSERT INTO `city` VALUES (1221, 'PADANG LAWAS');
INSERT INTO `city` VALUES (1222, 'LABUHAN BATU SELATAN');
INSERT INTO `city` VALUES (1223, 'LABUHAN BATU UTARA');
INSERT INTO `city` VALUES (1224, 'NIAS UTARA');
INSERT INTO `city` VALUES (1225, 'NIAS BARAT');
INSERT INTO `city` VALUES (1271, 'SIBOLGA');
INSERT INTO `city` VALUES (1272, 'TANJUNG BALAI');
INSERT INTO `city` VALUES (1273, 'PEMATANG SIANTAR');
INSERT INTO `city` VALUES (1274, 'TEBING TINGGI');
INSERT INTO `city` VALUES (1275, 'MEDAN');
INSERT INTO `city` VALUES (1276, 'BINJAI');
INSERT INTO `city` VALUES (1277, 'PADANGSIDIMPUAN');
INSERT INTO `city` VALUES (1278, 'GUNUNGSITOLI');
INSERT INTO `city` VALUES (1301, 'KEPULAUAN MENTAWAI');
INSERT INTO `city` VALUES (1302, 'PESISIR SELATAN');
INSERT INTO `city` VALUES (1303, 'SOLOK');
INSERT INTO `city` VALUES (1304, 'SIJUNJUNG');
INSERT INTO `city` VALUES (1305, 'TANAH DATAR');
INSERT INTO `city` VALUES (1306, 'PADANG PARIAMAN');
INSERT INTO `city` VALUES (1307, 'AGAM');
INSERT INTO `city` VALUES (1308, 'LIMA PULUH KOTA');
INSERT INTO `city` VALUES (1309, 'PASAMAN');
INSERT INTO `city` VALUES (1310, 'SOLOK SELATAN');
INSERT INTO `city` VALUES (1311, 'DHARMASRAYA');
INSERT INTO `city` VALUES (1312, 'PASAMAN BARAT');
INSERT INTO `city` VALUES (1371, 'PADANG');
INSERT INTO `city` VALUES (1372, 'SOLOK');
INSERT INTO `city` VALUES (1373, 'SAWAH LUNTO');
INSERT INTO `city` VALUES (1374, 'PADANG PANJANG');
INSERT INTO `city` VALUES (1375, 'BUKITTINGGI');
INSERT INTO `city` VALUES (1376, 'PAYAKUMBUH');
INSERT INTO `city` VALUES (1377, 'PARIAMAN');
INSERT INTO `city` VALUES (1401, 'KUANTAN SINGINGI');
INSERT INTO `city` VALUES (1402, 'INDRAGIRI HULU');
INSERT INTO `city` VALUES (1403, 'INDRAGIRI HILIR');
INSERT INTO `city` VALUES (1404, 'PELALAWAN');
INSERT INTO `city` VALUES (1405, 'S I A K');
INSERT INTO `city` VALUES (1406, 'KAMPAR');
INSERT INTO `city` VALUES (1407, 'ROKAN HULU');
INSERT INTO `city` VALUES (1408, 'BENGKALIS');
INSERT INTO `city` VALUES (1409, 'ROKAN HILIR');
INSERT INTO `city` VALUES (1410, 'KEPULAUAN MERANTI');
INSERT INTO `city` VALUES (1471, 'PEKANBARU');
INSERT INTO `city` VALUES (1473, 'D U M A I');
INSERT INTO `city` VALUES (1501, 'KERINCI');
INSERT INTO `city` VALUES (1502, 'MERANGIN');
INSERT INTO `city` VALUES (1503, 'SAROLANGUN');
INSERT INTO `city` VALUES (1504, 'BATANG HARI');
INSERT INTO `city` VALUES (1505, 'MUARO JAMBI');
INSERT INTO `city` VALUES (1506, 'TANJUNG JABUNG TIMUR');
INSERT INTO `city` VALUES (1507, 'TANJUNG JABUNG BARAT');
INSERT INTO `city` VALUES (1508, 'TEBO');
INSERT INTO `city` VALUES (1509, 'BUNGO');
INSERT INTO `city` VALUES (1571, 'JAMBI');
INSERT INTO `city` VALUES (1572, 'SUNGAI PENUH');
INSERT INTO `city` VALUES (1601, 'OGAN KOMERING ULU');
INSERT INTO `city` VALUES (1602, 'OGAN KOMERING ILIR');
INSERT INTO `city` VALUES (1603, 'MUARA ENIM');
INSERT INTO `city` VALUES (1604, 'LAHAT');
INSERT INTO `city` VALUES (1605, 'MUSI RAWAS');
INSERT INTO `city` VALUES (1606, 'MUSI BANYUASIN');
INSERT INTO `city` VALUES (1607, 'BANYU ASIN');
INSERT INTO `city` VALUES (1608, 'OGAN KOMERING ULU SELATAN');
INSERT INTO `city` VALUES (1609, 'OGAN KOMERING ULU TIMUR');
INSERT INTO `city` VALUES (1610, 'OGAN ILIR');
INSERT INTO `city` VALUES (1611, 'EMPAT LAWANG');
INSERT INTO `city` VALUES (1612, 'PENUKAL ABAB LEMATANG ILIR');
INSERT INTO `city` VALUES (1613, 'MUSI RAWAS UTARA');
INSERT INTO `city` VALUES (1671, 'PALEMBANG');
INSERT INTO `city` VALUES (1672, 'PRABUMULIH');
INSERT INTO `city` VALUES (1673, 'PAGAR ALAM');
INSERT INTO `city` VALUES (1674, 'LUBUKLINGGAU');
INSERT INTO `city` VALUES (1701, 'BENGKULU SELATAN');
INSERT INTO `city` VALUES (1702, 'REJANG LEBONG');
INSERT INTO `city` VALUES (1703, 'BENGKULU UTARA');
INSERT INTO `city` VALUES (1704, 'KAUR');
INSERT INTO `city` VALUES (1705, 'SELUMA');
INSERT INTO `city` VALUES (1706, 'MUKOMUKO');
INSERT INTO `city` VALUES (1707, 'LEBONG');
INSERT INTO `city` VALUES (1708, 'KEPAHIANG');
INSERT INTO `city` VALUES (1709, 'BENGKULU TENGAH');
INSERT INTO `city` VALUES (1771, 'BENGKULU');
INSERT INTO `city` VALUES (1801, 'LAMPUNG BARAT');
INSERT INTO `city` VALUES (1802, 'TANGGAMUS');
INSERT INTO `city` VALUES (1803, 'LAMPUNG SELATAN');
INSERT INTO `city` VALUES (1804, 'LAMPUNG TIMUR');
INSERT INTO `city` VALUES (1805, 'LAMPUNG TENGAH');
INSERT INTO `city` VALUES (1806, 'LAMPUNG UTARA');
INSERT INTO `city` VALUES (1807, 'WAY KANAN');
INSERT INTO `city` VALUES (1808, 'TULANGBAWANG');
INSERT INTO `city` VALUES (1809, 'PESAWARAN');
INSERT INTO `city` VALUES (1810, 'PRINGSEWU');
INSERT INTO `city` VALUES (1811, 'MESUJI');
INSERT INTO `city` VALUES (1812, 'TULANG BAWANG BARAT');
INSERT INTO `city` VALUES (1813, 'PESISIR BARAT');
INSERT INTO `city` VALUES (1871, 'BANDAR LAMPUNG');
INSERT INTO `city` VALUES (1872, 'METRO');
INSERT INTO `city` VALUES (1901, 'BANGKA');
INSERT INTO `city` VALUES (1902, 'BELITUNG');
INSERT INTO `city` VALUES (1903, 'BANGKA BARAT');
INSERT INTO `city` VALUES (1904, 'BANGKA TENGAH');
INSERT INTO `city` VALUES (1905, 'BANGKA SELATAN');
INSERT INTO `city` VALUES (1906, 'BELITUNG TIMUR');
INSERT INTO `city` VALUES (1971, 'PANGKAL PINANG');
INSERT INTO `city` VALUES (2101, 'KARIMUN');
INSERT INTO `city` VALUES (2102, 'BINTAN');
INSERT INTO `city` VALUES (2103, 'NATUNA');
INSERT INTO `city` VALUES (2104, 'LINGGA');
INSERT INTO `city` VALUES (2105, 'KEPULAUAN ANAMBAS');
INSERT INTO `city` VALUES (2171, 'B A T A M');
INSERT INTO `city` VALUES (2172, 'TANJUNG PINANG');
INSERT INTO `city` VALUES (3101, 'KEPULAUAN SERIBU');
INSERT INTO `city` VALUES (3171, 'JAKARTA SELATAN');
INSERT INTO `city` VALUES (3172, 'JAKARTA TIMUR');
INSERT INTO `city` VALUES (3173, 'JAKARTA PUSAT');
INSERT INTO `city` VALUES (3174, 'JAKARTA BARAT');
INSERT INTO `city` VALUES (3175, 'JAKARTA UTARA');
INSERT INTO `city` VALUES (3201, 'BOGOR');
INSERT INTO `city` VALUES (3202, 'SUKABUMI');
INSERT INTO `city` VALUES (3203, 'CIANJUR');
INSERT INTO `city` VALUES (3204, 'BANDUNG');
INSERT INTO `city` VALUES (3205, 'GARUT');
INSERT INTO `city` VALUES (3206, 'TASIKMALAYA');
INSERT INTO `city` VALUES (3207, 'CIAMIS');
INSERT INTO `city` VALUES (3208, 'KUNINGAN');
INSERT INTO `city` VALUES (3209, 'CIREBON');
INSERT INTO `city` VALUES (3210, 'MAJALENGKA');
INSERT INTO `city` VALUES (3211, 'SUMEDANG');
INSERT INTO `city` VALUES (3212, 'INDRAMAYU');
INSERT INTO `city` VALUES (3213, 'SUBANG');
INSERT INTO `city` VALUES (3214, 'PURWAKARTA');
INSERT INTO `city` VALUES (3215, 'KARAWANG');
INSERT INTO `city` VALUES (3216, 'BEKASI');
INSERT INTO `city` VALUES (3217, 'BANDUNG BARAT');
INSERT INTO `city` VALUES (3218, 'PANGANDARAN');
INSERT INTO `city` VALUES (3271, 'BOGOR');
INSERT INTO `city` VALUES (3272, 'SUKABUMI');
INSERT INTO `city` VALUES (3273, 'BANDUNG');
INSERT INTO `city` VALUES (3274, 'CIREBON');
INSERT INTO `city` VALUES (3275, 'BEKASI');
INSERT INTO `city` VALUES (3276, 'DEPOK');
INSERT INTO `city` VALUES (3277, 'CIMAHI');
INSERT INTO `city` VALUES (3278, 'TASIKMALAYA');
INSERT INTO `city` VALUES (3279, 'BANJAR');
INSERT INTO `city` VALUES (3301, 'CILACAP');
INSERT INTO `city` VALUES (3302, 'BANYUMAS');
INSERT INTO `city` VALUES (3303, 'PURBALINGGA');
INSERT INTO `city` VALUES (3304, 'BANJARNEGARA');
INSERT INTO `city` VALUES (3305, 'KEBUMEN');
INSERT INTO `city` VALUES (3306, 'PURWOREJO');
INSERT INTO `city` VALUES (3307, 'WONOSOBO');
INSERT INTO `city` VALUES (3308, 'MAGELANG');
INSERT INTO `city` VALUES (3309, 'BOYOLALI');
INSERT INTO `city` VALUES (3310, 'KLATEN');
INSERT INTO `city` VALUES (3311, 'SUKOHARJO');
INSERT INTO `city` VALUES (3312, 'WONOGIRI');
INSERT INTO `city` VALUES (3313, 'KARANGANYAR');
INSERT INTO `city` VALUES (3314, 'SRAGEN');
INSERT INTO `city` VALUES (3315, 'GROBOGAN');
INSERT INTO `city` VALUES (3316, 'BLORA');
INSERT INTO `city` VALUES (3317, 'REMBANG');
INSERT INTO `city` VALUES (3318, 'PATI');
INSERT INTO `city` VALUES (3319, 'KUDUS');
INSERT INTO `city` VALUES (3320, 'JEPARA');
INSERT INTO `city` VALUES (3321, 'DEMAK');
INSERT INTO `city` VALUES (3322, 'SEMARANG');
INSERT INTO `city` VALUES (3323, 'TEMANGGUNG');
INSERT INTO `city` VALUES (3324, 'KENDAL');
INSERT INTO `city` VALUES (3325, 'BATANG');
INSERT INTO `city` VALUES (3326, 'PEKALONGAN');
INSERT INTO `city` VALUES (3327, 'PEMALANG');
INSERT INTO `city` VALUES (3328, 'TEGAL');
INSERT INTO `city` VALUES (3329, 'BREBES');
INSERT INTO `city` VALUES (3371, 'MAGELANG');
INSERT INTO `city` VALUES (3372, 'SURAKARTA');
INSERT INTO `city` VALUES (3373, 'SALATIGA');
INSERT INTO `city` VALUES (3374, 'SEMARANG');
INSERT INTO `city` VALUES (3375, 'PEKALONGAN');
INSERT INTO `city` VALUES (3376, 'TEGAL');
INSERT INTO `city` VALUES (3401, 'KULON PROGO');
INSERT INTO `city` VALUES (3402, 'BANTUL');
INSERT INTO `city` VALUES (3403, 'GUNUNG KIDUL');
INSERT INTO `city` VALUES (3404, 'SLEMAN');
INSERT INTO `city` VALUES (3471, 'YOGYAKARTA');
INSERT INTO `city` VALUES (3501, 'PACITAN');
INSERT INTO `city` VALUES (3502, 'PONOROGO');
INSERT INTO `city` VALUES (3503, 'TRENGGALEK');
INSERT INTO `city` VALUES (3504, 'TULUNGAGUNG');
INSERT INTO `city` VALUES (3505, 'BLITAR');
INSERT INTO `city` VALUES (3506, 'KEDIRI');
INSERT INTO `city` VALUES (3507, 'MALANG');
INSERT INTO `city` VALUES (3508, 'LUMAJANG');
INSERT INTO `city` VALUES (3509, 'JEMBER');
INSERT INTO `city` VALUES (3510, 'BANYUWANGI');
INSERT INTO `city` VALUES (3511, 'BONDOWOSO');
INSERT INTO `city` VALUES (3512, 'SITUBONDO');
INSERT INTO `city` VALUES (3513, 'PROBOLINGGO');
INSERT INTO `city` VALUES (3514, 'PASURUAN');
INSERT INTO `city` VALUES (3515, 'SIDOARJO');
INSERT INTO `city` VALUES (3516, 'MOJOKERTO');
INSERT INTO `city` VALUES (3517, 'JOMBANG');
INSERT INTO `city` VALUES (3518, 'NGANJUK');
INSERT INTO `city` VALUES (3519, 'MADIUN');
INSERT INTO `city` VALUES (3520, 'MAGETAN');
INSERT INTO `city` VALUES (3521, 'NGAWI');
INSERT INTO `city` VALUES (3522, 'BOJONEGORO');
INSERT INTO `city` VALUES (3523, 'TUBAN');
INSERT INTO `city` VALUES (3524, 'LAMONGAN');
INSERT INTO `city` VALUES (3525, 'GRESIK');
INSERT INTO `city` VALUES (3526, 'BANGKALAN');
INSERT INTO `city` VALUES (3527, 'SAMPANG');
INSERT INTO `city` VALUES (3528, 'PAMEKASAN');
INSERT INTO `city` VALUES (3529, 'SUMENEP');
INSERT INTO `city` VALUES (3571, 'KEDIRI');
INSERT INTO `city` VALUES (3572, 'BLITAR');
INSERT INTO `city` VALUES (3573, 'MALANG');
INSERT INTO `city` VALUES (3574, 'PROBOLINGGO');
INSERT INTO `city` VALUES (3575, 'PASURUAN');
INSERT INTO `city` VALUES (3576, 'MOJOKERTO');
INSERT INTO `city` VALUES (3577, 'MADIUN');
INSERT INTO `city` VALUES (3578, 'SURABAYA');
INSERT INTO `city` VALUES (3579, 'BATU');
INSERT INTO `city` VALUES (3601, 'PANDEGLANG');
INSERT INTO `city` VALUES (3602, 'LEBAK');
INSERT INTO `city` VALUES (3603, 'TANGERANG');
INSERT INTO `city` VALUES (3604, 'SERANG');
INSERT INTO `city` VALUES (3671, 'TANGERANG');
INSERT INTO `city` VALUES (3672, 'CILEGON');
INSERT INTO `city` VALUES (3673, 'SERANG');
INSERT INTO `city` VALUES (3674, 'TANGERANG SELATAN');
INSERT INTO `city` VALUES (5101, 'JEMBRANA');
INSERT INTO `city` VALUES (5102, 'TABANAN');
INSERT INTO `city` VALUES (5103, 'BADUNG');
INSERT INTO `city` VALUES (5104, 'GIANYAR');
INSERT INTO `city` VALUES (5105, 'KLUNGKUNG');
INSERT INTO `city` VALUES (5106, 'BANGLI');
INSERT INTO `city` VALUES (5107, 'KARANG ASEM');
INSERT INTO `city` VALUES (5108, 'BULELENG');
INSERT INTO `city` VALUES (5171, 'DENPASAR');
INSERT INTO `city` VALUES (5201, 'LOMBOK BARAT');
INSERT INTO `city` VALUES (5202, 'LOMBOK TENGAH');
INSERT INTO `city` VALUES (5203, 'LOMBOK TIMUR');
INSERT INTO `city` VALUES (5204, 'SUMBAWA');
INSERT INTO `city` VALUES (5205, 'DOMPU');
INSERT INTO `city` VALUES (5206, 'BIMA');
INSERT INTO `city` VALUES (5207, 'SUMBAWA BARAT');
INSERT INTO `city` VALUES (5208, 'LOMBOK UTARA');
INSERT INTO `city` VALUES (5271, 'MATARAM');
INSERT INTO `city` VALUES (5272, 'BIMA');
INSERT INTO `city` VALUES (5301, 'SUMBA BARAT');
INSERT INTO `city` VALUES (5302, 'SUMBA TIMUR');
INSERT INTO `city` VALUES (5303, 'KUPANG');
INSERT INTO `city` VALUES (5304, 'TIMOR TENGAH SELATAN');
INSERT INTO `city` VALUES (5305, 'TIMOR TENGAH UTARA');
INSERT INTO `city` VALUES (5306, 'BELU');
INSERT INTO `city` VALUES (5307, 'ALOR');
INSERT INTO `city` VALUES (5308, 'LEMBATA');
INSERT INTO `city` VALUES (5309, 'FLORES TIMUR');
INSERT INTO `city` VALUES (5310, 'SIKKA');
INSERT INTO `city` VALUES (5311, 'ENDE');
INSERT INTO `city` VALUES (5312, 'NGADA');
INSERT INTO `city` VALUES (5313, 'MANGGARAI');
INSERT INTO `city` VALUES (5314, 'ROTE NDAO');
INSERT INTO `city` VALUES (5315, 'MANGGARAI BARAT');
INSERT INTO `city` VALUES (5316, 'SUMBA TENGAH');
INSERT INTO `city` VALUES (5317, 'SUMBA BARAT DAYA');
INSERT INTO `city` VALUES (5318, 'NAGEKEO');
INSERT INTO `city` VALUES (5319, 'MANGGARAI TIMUR');
INSERT INTO `city` VALUES (5320, 'SABU RAIJUA');
INSERT INTO `city` VALUES (5321, 'MALAKA');
INSERT INTO `city` VALUES (5371, 'KUPANG');
INSERT INTO `city` VALUES (6101, 'SAMBAS');
INSERT INTO `city` VALUES (6102, 'BENGKAYANG');
INSERT INTO `city` VALUES (6103, 'LANDAK');
INSERT INTO `city` VALUES (6104, 'MEMPAWAH');
INSERT INTO `city` VALUES (6105, 'SANGGAU');
INSERT INTO `city` VALUES (6106, 'KETAPANG');
INSERT INTO `city` VALUES (6107, 'SINTANG');
INSERT INTO `city` VALUES (6108, 'KAPUAS HULU');
INSERT INTO `city` VALUES (6109, 'SEKADAU');
INSERT INTO `city` VALUES (6110, 'MELAWI');
INSERT INTO `city` VALUES (6111, 'KAYONG UTARA');
INSERT INTO `city` VALUES (6112, 'KUBU RAYA');
INSERT INTO `city` VALUES (6171, 'PONTIANAK');
INSERT INTO `city` VALUES (6172, 'SINGKAWANG');
INSERT INTO `city` VALUES (6201, 'KOTAWARINGIN BARAT');
INSERT INTO `city` VALUES (6202, 'KOTAWARINGIN TIMUR');
INSERT INTO `city` VALUES (6203, 'KAPUAS');
INSERT INTO `city` VALUES (6204, 'BARITO SELATAN');
INSERT INTO `city` VALUES (6205, 'BARITO UTARA');
INSERT INTO `city` VALUES (6206, 'SUKAMARA');
INSERT INTO `city` VALUES (6207, 'LAMANDAU');
INSERT INTO `city` VALUES (6208, 'SERUYAN');
INSERT INTO `city` VALUES (6209, 'KATINGAN');
INSERT INTO `city` VALUES (6210, 'PULANG PISAU');
INSERT INTO `city` VALUES (6211, 'GUNUNG MAS');
INSERT INTO `city` VALUES (6212, 'BARITO TIMUR');
INSERT INTO `city` VALUES (6213, 'MURUNG RAYA');
INSERT INTO `city` VALUES (6271, 'PALANGKA RAYA');
INSERT INTO `city` VALUES (6301, 'TANAH LAUT');
INSERT INTO `city` VALUES (6302, 'KOTA BARU');
INSERT INTO `city` VALUES (6303, 'BANJAR');
INSERT INTO `city` VALUES (6304, 'BARITO KUALA');
INSERT INTO `city` VALUES (6305, 'TAPIN');
INSERT INTO `city` VALUES (6306, 'HULU SUNGAI SELATAN');
INSERT INTO `city` VALUES (6307, 'HULU SUNGAI TENGAH');
INSERT INTO `city` VALUES (6308, 'HULU SUNGAI UTARA');
INSERT INTO `city` VALUES (6309, 'TABALONG');
INSERT INTO `city` VALUES (6310, 'TANAH BUMBU');
INSERT INTO `city` VALUES (6311, 'BALANGAN');
INSERT INTO `city` VALUES (6371, 'BANJARMASIN');
INSERT INTO `city` VALUES (6372, 'BANJAR BARU');
INSERT INTO `city` VALUES (6401, 'PASER');
INSERT INTO `city` VALUES (6402, 'KUTAI BARAT');
INSERT INTO `city` VALUES (6403, 'KUTAI KARTANEGARA');
INSERT INTO `city` VALUES (6404, 'KUTAI TIMUR');
INSERT INTO `city` VALUES (6405, 'BERAU');
INSERT INTO `city` VALUES (6409, 'PENAJAM PASER UTARA');
INSERT INTO `city` VALUES (6411, 'MAHAKAM HULU');
INSERT INTO `city` VALUES (6471, 'BALIKPAPAN');
INSERT INTO `city` VALUES (6472, 'SAMARINDA');
INSERT INTO `city` VALUES (6474, 'BONTANG');
INSERT INTO `city` VALUES (6501, 'MALINAU');
INSERT INTO `city` VALUES (6502, 'BULUNGAN');
INSERT INTO `city` VALUES (6503, 'TANA TIDUNG');
INSERT INTO `city` VALUES (6504, 'NUNUKAN');
INSERT INTO `city` VALUES (6571, 'TARAKAN');
INSERT INTO `city` VALUES (7101, 'BOLAANG MONGONDOW');
INSERT INTO `city` VALUES (7102, 'MINAHASA');
INSERT INTO `city` VALUES (7103, 'KEPULAUAN SANGIHE');
INSERT INTO `city` VALUES (7104, 'KEPULAUAN TALAUD');
INSERT INTO `city` VALUES (7105, 'MINAHASA SELATAN');
INSERT INTO `city` VALUES (7106, 'MINAHASA UTARA');
INSERT INTO `city` VALUES (7107, 'BOLAANG MONGONDOW UTARA');
INSERT INTO `city` VALUES (7108, 'SIAU TAGULANDANG BIARO');
INSERT INTO `city` VALUES (7109, 'MINAHASA TENGGARA');
INSERT INTO `city` VALUES (7110, 'BOLAANG MONGONDOW SELATAN');
INSERT INTO `city` VALUES (7111, 'BOLAANG MONGONDOW TIMUR');
INSERT INTO `city` VALUES (7171, 'MANADO');
INSERT INTO `city` VALUES (7172, 'BITUNG');
INSERT INTO `city` VALUES (7173, 'TOMOHON');
INSERT INTO `city` VALUES (7174, 'KOTAMOBAGU');
INSERT INTO `city` VALUES (7201, 'BANGGAI KEPULAUAN');
INSERT INTO `city` VALUES (7202, 'BANGGAI');
INSERT INTO `city` VALUES (7203, 'MOROWALI');
INSERT INTO `city` VALUES (7204, 'POSO');
INSERT INTO `city` VALUES (7205, 'DONGGALA');
INSERT INTO `city` VALUES (7206, 'TOLI-TOLI');
INSERT INTO `city` VALUES (7207, 'BUOL');
INSERT INTO `city` VALUES (7208, 'PARIGI MOUTONG');
INSERT INTO `city` VALUES (7209, 'TOJO UNA-UNA');
INSERT INTO `city` VALUES (7210, 'SIGI');
INSERT INTO `city` VALUES (7211, 'BANGGAI LAUT');
INSERT INTO `city` VALUES (7212, 'MOROWALI UTARA');
INSERT INTO `city` VALUES (7271, 'PALU');
INSERT INTO `city` VALUES (7301, 'KEPULAUAN SELAYAR');
INSERT INTO `city` VALUES (7302, 'BULUKUMBA');
INSERT INTO `city` VALUES (7303, 'BANTAENG');
INSERT INTO `city` VALUES (7304, 'JENEPONTO');
INSERT INTO `city` VALUES (7305, 'TAKALAR');
INSERT INTO `city` VALUES (7306, 'GOWA');
INSERT INTO `city` VALUES (7307, 'SINJAI');
INSERT INTO `city` VALUES (7308, 'MAROS');
INSERT INTO `city` VALUES (7309, 'PANGKAJENE DAN KEPULAUAN');
INSERT INTO `city` VALUES (7310, 'BARRU');
INSERT INTO `city` VALUES (7311, 'BONE');
INSERT INTO `city` VALUES (7312, 'SOPPENG');
INSERT INTO `city` VALUES (7313, 'WAJO');
INSERT INTO `city` VALUES (7314, 'SIDENRENG RAPPANG');
INSERT INTO `city` VALUES (7315, 'PINRANG');
INSERT INTO `city` VALUES (7316, 'ENREKANG');
INSERT INTO `city` VALUES (7317, 'LUWU');
INSERT INTO `city` VALUES (7318, 'TANA TORAJA');
INSERT INTO `city` VALUES (7322, 'LUWU UTARA');
INSERT INTO `city` VALUES (7325, 'LUWU TIMUR');
INSERT INTO `city` VALUES (7326, 'TORAJA UTARA');
INSERT INTO `city` VALUES (7371, 'MAKASSAR');
INSERT INTO `city` VALUES (7372, 'PAREPARE');
INSERT INTO `city` VALUES (7373, 'PALOPO');
INSERT INTO `city` VALUES (7401, 'BUTON');
INSERT INTO `city` VALUES (7402, 'MUNA');
INSERT INTO `city` VALUES (7403, 'KONAWE');
INSERT INTO `city` VALUES (7404, 'KOLAKA');
INSERT INTO `city` VALUES (7405, 'KONAWE SELATAN');
INSERT INTO `city` VALUES (7406, 'BOMBANA');
INSERT INTO `city` VALUES (7407, 'WAKATOBI');
INSERT INTO `city` VALUES (7408, 'KOLAKA UTARA');
INSERT INTO `city` VALUES (7409, 'BUTON UTARA');
INSERT INTO `city` VALUES (7410, 'KONAWE UTARA');
INSERT INTO `city` VALUES (7411, 'KOLAKA TIMUR');
INSERT INTO `city` VALUES (7412, 'KONAWE KEPULAUAN');
INSERT INTO `city` VALUES (7413, 'MUNA BARAT');
INSERT INTO `city` VALUES (7414, 'BUTON TENGAH');
INSERT INTO `city` VALUES (7415, 'BUTON SELATAN');
INSERT INTO `city` VALUES (7471, 'KENDARI');
INSERT INTO `city` VALUES (7472, 'BAUBAU');
INSERT INTO `city` VALUES (7501, 'BOALEMO');
INSERT INTO `city` VALUES (7502, 'GORONTALO');
INSERT INTO `city` VALUES (7503, 'POHUWATO');
INSERT INTO `city` VALUES (7504, 'BONE BOLANGO');
INSERT INTO `city` VALUES (7505, 'GORONTALO UTARA');
INSERT INTO `city` VALUES (7571, 'GORONTALO');
INSERT INTO `city` VALUES (7601, 'MAJENE');
INSERT INTO `city` VALUES (7602, 'POLEWALI MANDAR');
INSERT INTO `city` VALUES (7603, 'MAMASA');
INSERT INTO `city` VALUES (7604, 'MAMUJU');
INSERT INTO `city` VALUES (7605, 'MAMUJU UTARA');
INSERT INTO `city` VALUES (7606, 'MAMUJU TENGAH');
INSERT INTO `city` VALUES (8101, 'MALUKU TENGGARA BARAT');
INSERT INTO `city` VALUES (8102, 'MALUKU TENGGARA');
INSERT INTO `city` VALUES (8103, 'MALUKU TENGAH');
INSERT INTO `city` VALUES (8104, 'BURU');
INSERT INTO `city` VALUES (8105, 'KEPULAUAN ARU');
INSERT INTO `city` VALUES (8106, 'SERAM BAGIAN BARAT');
INSERT INTO `city` VALUES (8107, 'SERAM BAGIAN TIMUR');
INSERT INTO `city` VALUES (8108, 'MALUKU BARAT DAYA');
INSERT INTO `city` VALUES (8109, 'BURU SELATAN');
INSERT INTO `city` VALUES (8171, 'AMBON');
INSERT INTO `city` VALUES (8172, 'TUAL');
INSERT INTO `city` VALUES (8201, 'HALMAHERA BARAT');
INSERT INTO `city` VALUES (8202, 'HALMAHERA TENGAH');
INSERT INTO `city` VALUES (8203, 'KEPULAUAN SULA');
INSERT INTO `city` VALUES (8204, 'HALMAHERA SELATAN');
INSERT INTO `city` VALUES (8205, 'HALMAHERA UTARA');
INSERT INTO `city` VALUES (8206, 'HALMAHERA TIMUR');
INSERT INTO `city` VALUES (8207, 'PULAU MOROTAI');
INSERT INTO `city` VALUES (8208, 'PULAU TALIABU');
INSERT INTO `city` VALUES (8271, 'TERNATE');
INSERT INTO `city` VALUES (8272, 'TIDORE KEPULAUAN');
INSERT INTO `city` VALUES (9101, 'FAKFAK');
INSERT INTO `city` VALUES (9102, 'KAIMANA');
INSERT INTO `city` VALUES (9103, 'TELUK WONDAMA');
INSERT INTO `city` VALUES (9104, 'TELUK BINTUNI');
INSERT INTO `city` VALUES (9105, 'MANOKWARI');
INSERT INTO `city` VALUES (9106, 'SORONG SELATAN');
INSERT INTO `city` VALUES (9107, 'SORONG');
INSERT INTO `city` VALUES (9108, 'RAJA AMPAT');
INSERT INTO `city` VALUES (9109, 'TAMBRAUW');
INSERT INTO `city` VALUES (9110, 'MAYBRAT');
INSERT INTO `city` VALUES (9111, 'MANOKWARI SELATAN');
INSERT INTO `city` VALUES (9112, 'PEGUNUNGAN ARFAK');
INSERT INTO `city` VALUES (9171, 'SORONG');
INSERT INTO `city` VALUES (9401, 'MERAUKE');
INSERT INTO `city` VALUES (9402, 'JAYAWIJAYA');
INSERT INTO `city` VALUES (9403, 'JAYAPURA');
INSERT INTO `city` VALUES (9404, 'NABIRE');
INSERT INTO `city` VALUES (9408, 'KEPULAUAN YAPEN');
INSERT INTO `city` VALUES (9409, 'BIAK NUMFOR');
INSERT INTO `city` VALUES (9410, 'PANIAI');
INSERT INTO `city` VALUES (9411, 'PUNCAK JAYA');
INSERT INTO `city` VALUES (9412, 'MIMIKA');
INSERT INTO `city` VALUES (9413, 'BOVEN DIGOEL');
INSERT INTO `city` VALUES (9414, 'MAPPI');
INSERT INTO `city` VALUES (9415, 'ASMAT');
INSERT INTO `city` VALUES (9416, 'YAHUKIMO');
INSERT INTO `city` VALUES (9417, 'PEGUNUNGAN BINTANG');
INSERT INTO `city` VALUES (9418, 'TOLIKARA');
INSERT INTO `city` VALUES (9419, 'SARMI');
INSERT INTO `city` VALUES (9420, 'KEEROM');
INSERT INTO `city` VALUES (9426, 'WAROPEN');
INSERT INTO `city` VALUES (9427, 'SUPIORI');
INSERT INTO `city` VALUES (9428, 'MAMBERAMO RAYA');
INSERT INTO `city` VALUES (9429, 'NDUGA');
INSERT INTO `city` VALUES (9430, 'LANNY JAYA');
INSERT INTO `city` VALUES (9431, 'MAMBERAMO TENGAH');
INSERT INTO `city` VALUES (9432, 'YALIMO');
INSERT INTO `city` VALUES (9433, 'PUNCAK');
INSERT INTO `city` VALUES (9434, 'DOGIYAI');
INSERT INTO `city` VALUES (9435, 'INTAN JAYA');
INSERT INTO `city` VALUES (9436, 'DEIYAI');
INSERT INTO `city` VALUES (9471, 'JAYAPURA');

-- ----------------------------
-- Table structure for guestbook
-- ----------------------------
DROP TABLE IF EXISTS `guestbook`;
CREATE TABLE `guestbook`  (
  `id_guest` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_province` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_guest`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES (11, 'ACEH');
INSERT INTO `province` VALUES (12, 'SUMATERA UTARA');
INSERT INTO `province` VALUES (13, 'SUMATERA BARAT');
INSERT INTO `province` VALUES (14, 'RIAU');
INSERT INTO `province` VALUES (15, 'JAMBI');
INSERT INTO `province` VALUES (16, 'SUMATERA SELATAN');
INSERT INTO `province` VALUES (17, 'BENGKULU');
INSERT INTO `province` VALUES (18, 'LAMPUNG');
INSERT INTO `province` VALUES (19, 'KEP. BANGKA BELITUNG');
INSERT INTO `province` VALUES (21, 'KEP. RIAU');
INSERT INTO `province` VALUES (31, 'DKI JAKARTA');
INSERT INTO `province` VALUES (32, 'JAWA BARAT');
INSERT INTO `province` VALUES (33, 'JAWA TENGAH');
INSERT INTO `province` VALUES (34, 'DI YOGYAKARTA');
INSERT INTO `province` VALUES (35, 'JAWA TIMUR');
INSERT INTO `province` VALUES (36, 'BANTEN');
INSERT INTO `province` VALUES (51, 'BALI');
INSERT INTO `province` VALUES (52, 'NUSA TENGGARA BARAT');
INSERT INTO `province` VALUES (53, 'NUSA TENGGARA TIMUR');
INSERT INTO `province` VALUES (61, 'KALIMANTAN BARAT');
INSERT INTO `province` VALUES (62, 'KALIMANTAN TENGAH');
INSERT INTO `province` VALUES (63, 'KALIMANTAN SELATAN');
INSERT INTO `province` VALUES (64, 'KALIMANTAN TIMUR');
INSERT INTO `province` VALUES (65, 'KALIMANTAN UTARA');
INSERT INTO `province` VALUES (71, 'SULAWESI UTARA');
INSERT INTO `province` VALUES (72, 'SULAWESI TENGAH');
INSERT INTO `province` VALUES (73, 'SULAWESI SELATAN');
INSERT INTO `province` VALUES (74, 'SULAWESI TENGGARA');
INSERT INTO `province` VALUES (75, 'GORONTALO');
INSERT INTO `province` VALUES (76, 'SULAWESI BARAT');
INSERT INTO `province` VALUES (81, 'MALUKU');
INSERT INTO `province` VALUES (82, 'MALUKU UTARA');
INSERT INTO `province` VALUES (91, 'PAPUA BARAT');
INSERT INTO `province` VALUES (94, 'PAPUA');

SET FOREIGN_KEY_CHECKS = 1;
