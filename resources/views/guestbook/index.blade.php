@extends('template')
@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>List Guestbook</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">List Guestbook</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="/guestbook/add" class="btn btn-primary btn-block">Add Guestbook</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Organization</th>
                                    <th>Address</th>
                                    <th>Province</th>
                                    <th>City</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($guestbook as $no => $g)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $g->first_name ?? '' }}</td>
                                        <td>{{ $g->last_name ?? '' }}</td>
                                        <td>{{ $g->organization ?? '' }}</td>
                                        <td>{{ $g->address ?? '' }}</td>
                                        <td>{{ $g->province->name ?? '' }}</td>
                                        <td>{{ $g->city->name ?? '' }}</td>
                                        <td style="text-align:center">
                                            <a href="/guestbook/{{$g->id_guest}}/edit" onclick="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt text-success" aria-hidden="true"></i></a>
                                            <a onclick="deleteData('{{$g->id_guest}}')" id="delete" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script>
    function deleteData(id) {
        var confirmAction = confirm('Are you sure delete this data ? ');
        if (confirmAction == true) {
            window.location.href = "/guestbook/"+id+"/delete";
        } 
    }
</script>
@endsection
