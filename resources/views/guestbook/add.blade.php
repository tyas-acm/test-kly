@extends('template')

@section('style')
<link href="{{ asset('admin/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    .error {
        color: red;
    }

    .success {
        color: green;
    }
</style>
@endsection

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Add Guestbook</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/guestbook">Guestbook</a></li>
                <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
             
              <!-- form start -->
              <form id="form1" method="post" action="/guestbook/add_action">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <label for="organization">Organization</label>
                                <input type="text" name="organization" class="form-control" id="organization" placeholder="Organization">
                            </div>
                            <div class="form-group">
                                <label for="time">Province</label>
                                <select class="form-control required" name="province" id="province">
                                    <option value="" hidden selected disable></option>
                                    @foreach ( $province as $p )
                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" name="address" class="form-control" id="address" placeholder="Address">
                            </div>
                            <div class="form-group">
                                <label for="city">City</label>
                                <select class="form-control required" name="city" id="city">
                                    <option value="" hidden selected disable>Choose City</option>
                                    <!-- @foreach ( $city as $c )
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                    @endforeach -->
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- /.card-body -->

                <div class="card-footer text-right">
                    <button type="submit" id="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          

        </div>
      </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src="{{ asset('admin/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('admin/plugins/select2/js/select2.min.js') }}"></script>
<script>
    // $('#submit').unbind('click').bind('click', function confirmClose(e){
    //     if(!confirm('Are you sure save this data ? '))
    //         e.preventDefault();
    // });

    $('#province').select2({
        placeholder: "Choose Province",
        allowClear: true
    });

    $('#city').select2({
        placeholder: "Choose City",
        allowClear: true
    });

    $('#province').change(function(){
        var idProv = $('#province').children('option:selected').val();

        $.ajax({
            type: "GET",
            url: "/guestbook/"+idProv+"/get_city",
            dataType: "json",
            asyn: true,
            success: function(response){
                data = Object.entries(response);
                // console.log(data);
                var row;
                for(let x = 0;x < data.length; x++) {
                    row += '<option value="'+data[x][1].id+'">'+data[x][1].name+'</option>'
                }

                $("select[name='city']").html('<option value="" hidden selected disable>Choose City</option>'+row);
            }
        });
    });

    $( document ).ready( function () {
        $( "#form1" ).validate( {
            rules: {
                first_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                },
                last_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                },
                organization: {
                    required: true,
                    minlength: 5,
                    maxlength: 50
                },
                address: {
                    required: true,
                    minlength: 5,
                    maxlength: 50
                },
                province: {
                    required: true
                },
                city: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "Nama depan harus diisi",
                    minlength: "Nama depan minimal 2 karakter",
                    maxlength: "Nama depan maksimal 50 karakter"
                },
                last_name: {
                    required: "Nama belakang harus diisi",
                    minlength: "Nama belakang minimal 2 karakter",
                    maxlength: "Nama belakang maksimal 30 karakter"
                },
                organization: {
                    required: "Organisasi harus diisi",
                    minlength: "Organisasi minimal 5 karakter",
                    maxlength: "Organisasi maksimal 50 karakter"
                },
                address: {
                    required: "Alamat harus diisi",
                    minlength: "Alamat minimal 5 karakter",
                    maxlength: "Alamat maksimal 50 karakter"
                },
                province: {
                    required: "Provinsi harus dipilih"
                },
                city: {
                    required: "Kota harus dipilih"
                }
            },
            errorElement: "small",
            errorPlacement: function ( error, element ) {
                // Add the `help-block` class to the error element
                error.addClass( "help-block" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.parent( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            submitHandler: function(form) {
                if(confirm('Are you sure save this data ?')) {
                    form.submit();
                }
            }
        } );
    });
</script>

@endsection
