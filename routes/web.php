<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', 'DashboardController@index');

Route::get  ('/', 'LoginController@index');
Route::get  ('/logout', 'LoginController@logout');
Route::post ('/authenticating', 'LoginController@authenticating');

// GUESTBOOK
Route::get  ('/guestbook', 'GuestbookController@index');
Route::get  ('/guestbook/add', 'GuestbookController@add');
Route::post ('/guestbook/add_action', 'GuestbookController@add_action');
Route::get  ('/guestbook/{id}/edit', 'GuestbookController@edit');
Route::post ('/guestbook/edit_action', 'GuestbookController@edit_action');
Route::get  ('/guestbook/{id}/delete', 'GuestbookController@delete');

Route::get  ('/guestbook/{id}/get_city', 'GuestbookController@get_city');
