<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    //** LOGIN PAGE */
	public function index(Request $request) {
		return view('/login/login');
	}

	//** AUTHENTICATING ACTION */		
	Public function authenticating(Request $request) {

		// -- Check email and password user
		// $user = User::where('email',$request->email)->first();

		if($request->email == 'admin@mail.com') {
            $status_login =  $request->password == 'admin' ? 1 : 0;
            $user_type = 'admin';
		} else {
            $status_login = 0;
        }
		
		// -- if success 
		if ($status_login == 1 ) { 	

            // -- create session
            if($user_type == 'admin') {
                $request->session()->put('name', 'Admin');
                $request->session()->put('user_type', 'admin');
                
                return redirect('/dashboard')->with('success', 'Welcome Admin');
            }
			
		} else {
			
			return redirect('/')->with('error', 'Email atau password salah');  	
		} 			

	}

	//** LOGOUT ACTION */		
	Public function logout(Request $request) {
		$request->session()->put('id','');
		$request->session()->put('name', '');
		$request->session()->put('user_type', '');

		return redirect('/')->with('success', 'Logout success');  	
	}


}


