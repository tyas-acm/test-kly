<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guestbook;
use App\Province;
use App\City;

class GuestbookController extends Controller
{

    //** GUESTBOOK PAGE */
    public function index(){
        $guestbook = Guestbook::get();

        return view('/guestbook/index', [
            'guestbook' => $guestbook
        ]);
    }

    //** ADD GUESTBOOK */
    public function add(){
        $province = Province::get();
        $city = City::get();

        return view('/guestbook/add', [
            'province' => $province,
            'city' => $city
        ]);
    }

    //** ADD ACTION */
    public function add_action(Request $request){

        Guestbook::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'organization' => $request->organization,
            'address' => $request->address,
            'id_province' => $request->province,
            'id_city' => $request->city
        ]);

        return redirect('/guestbook')->with('success', 'Success to add guestbook');
    }

    //** EDIT GUESTBOOK */
    public function edit($id){
        $guestbook = Guestbook::where('id_guest', $id)->first();
        $province = Province::get();
        $city = City::get();

        return view('/guestbook/edit', [
            'guestbook' => $guestbook,
            'province' => $province,
            'city' => $city
        ]);
    }

    //** EDIT ACTION */
    public function edit_action(Request $request){

        Guestbook::where('id_guest', $request->id_guest)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'organization' => $request->organization,
            'address' => $request->address,
            'id_province' => $request->province,
            'id_city' => $request->city
        ]);

        return redirect('/guestbook')->with('success', 'Update Success');
    }

    //** DELETE GUESTBOOK */
    public function delete($id){
        $guestbook = Guestbook::where('id_guest', $id);
        $guestbook->delete();

        return redirect('/guestbook')->with('success', 'Delete Success');
    }

    public function get_city($id){
        $city = City::get();
        $citys = [];

        foreach($city as $c){
            $cid = substr($c->id, 0, 2);
            if($cid == $id){
                array_push($citys, [
                    'id' => $c->id,
                    'name' => $c->name
                ]);
            }
        }

        return $citys;
    }

}
