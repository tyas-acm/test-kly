<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guestbook extends Model
{
    protected $table = 'guestbook';
    protected $primaryKey = 'id_guest';
    public $incrementing = false;
    protected $fillable = [
        'id_guest',
        'first_name',
        'last_name',
        'organization',
        'address',
        'id_province', 
        'id_city'
    ];

    public function province(){
		return $this->belongsTo('App\Province', 'id_province', 'id');
	}

    public function city(){
		return $this->belongsTo('App\City', 'id_city', 'id');
	}
}
